call plug#begin('~/.config/nvim/bundle')

" Plug 'christoomey/vim-tmux-navigator'
" Plug 'ryanoasis/vim-webdevicons'
" Plug 'benekastah/neomake'
Plug 'neomake/neomake'
Plug 'tpope/vim-commentary'
" Plug 'nelstrom/vim-visual-star-search'
Plug 'mileszs/ack.vim'
Plug 'kien/ctrlp.vim'
" Plug 'd11wtq/ctrlp_bdelete.vim'
Plug 'Raimondi/delimitMate'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-fugitive'
" Plug 'scrooloose/nerdtree'
Plug 'airblade/vim-gitgutter'
Plug 'bling/vim-airline'
" Plug 'duff/vim-bufonly'
Plug 'gregsexton/MatchTag'
" Plug 'Shougo/deoplete.nvim'
" Plug 'Shougo/neosnippet'
" Plug 'honza/vim-snippets'
Plug 'othree/html5.vim'
" Plug 'xsbeats/vim-blade'
Plug 'elzr/vim-json'
" Plug 'evidens/vim-twig'
Plug 'majutsushi/tagbar'
Plug 'jelera/vim-javascript-syntax'
Plug 'pangloss/vim-javascript'
Plug 'StanAngeloff/php.vim'
" Plug 'stephpy/vim-yaml'
Plug 'cakebaker/scss-syntax.vim'
" Plug 'kchmck/vim-coffee-script'
" Plug 'mustache/vim-mustache-handlebars'
" Plug 'mhinz/vim-startify'
Plug 'vim-airline/vim-airline-themes'
" Plug 'benjie/neomake-local-eslint.vim'

Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'
" Plug 'altercation/vim-colors-solarized'
Plug 'iCyMind/NeoSolarized'
" Plug 'vim-scripts/YankRing.vim'
Plug 'DavidEGx/ctrlp-smarttabs'
Plug 'Lokaltog/vim-easymotion'
Plug 'tacahiroy/ctrlp-funky'
Plug 'tpope/vim-unimpaired'
" Plug 'm2mdas/phpcomplete-extended'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'evidens/vim-twig'
Plug 'mklabs/split-term.vim'
" Plug 'scrooloose/nerdtree'

" Plug 'git://drupalcode.org/project/vimrc.git', {'rtp': 'bundle/vim-plugin-for-drupal/'}
" Plug 'd11wtq/ctrlp_bdelete.vim'

call plug#end()                                                               "Finish Vundle initialization

filetype plugin indent on                                                       "Enable plugins and indents by filetype

" let g:mapleader = ","                                                           "Change leader to a comma

let g:enable_bold_font = 1                                                      "Enable bold font in colorscheme

" ================ General Config ====================

set t_Co=256                                                                    "Set 256 colors
set title                                                                       "change the terminal's title
set number                                                                      "Line numbers are good
set history=50                                                                 "Store lots of :cmdline history
set showcmd                                                                     "Show incomplete cmds down the bottom
set noshowmode                                                                  "Hide showmode because of the powerline plugin
set gdefault                                                                    "Set global flag for search and replace
set gcr=a:blinkon500-blinkwait500-blinkoff500                                   "Set cursor blinking rate
set cursorline                                                                  "Highlight current line
set smartcase                                                                   "Smart case search if there is uppercase
set ignorecase                                                                  "case insensitive search
set showmatch                                                                   "Highlight matching bracket
set nostartofline                                                               "Jump to first non-blank character
set timeoutlen=1000 ttimeoutlen=200                                             "Reduce Command timeout for faster escape and O
set fileencoding=utf-8                                                          "Set utf-8 encoding on write
set wrap                                                                        "Enable word wrap
set linebreak                                                                   "Wrap lines at convenient points
set listchars=tab:\ \ ,trail:·                                                  "Set trails for tabs and spaces
set list                                                                        "Enable listchars
set lazyredraw                                                                  "Do not redraw on registers and macros
set completeopt-=preview                                                        "Disable preview for autocomplete
set background=dark                                                             "Set background to dark
set hidden                                                                      "Hide buffers in background
set conceallevel=2 concealcursor=i                                              "neosnippets conceal marker
set splitright                                                                  "Set up new splits positions

syntax on                                                                       "turn on syntax highlighting

" colorscheme solarized

" default value is "normal", Setting this option to "high" or "low" does use
" the
" " same Solarized palette but simply shifts some values up or down in order
" to
" " expand or compress the tonal range displayed.
let g:neosolarized_contrast = "normal"
"
" " Special characters such as trailing whitespace, tabs, newlines, when
" displayed
" " using ":set list" can be set to one of three levels depending on your
" needs.
" " Default value is "normal". Provide "high" and "low" options.
let g:neosolarized_visibility = "normal"
"
" " If you wish to enable/disable NeoSolarized from displaying bold,
" underlined or italicized
" " typefaces, simply assign 1 or 0 to the appropriate variable. Default
" values:
let g:neosolarized_bold = 1
let g:neosolarized_underline = 1
let g:neosolarized_italic = 1

colorscheme NeoSolarized

" ================ Turn Off Swap Files ==============

set noswapfile
set nobackup
set nowb

" ================ Persistent Undo ==================

" Keep undo history across sessions, by storing in file.
" silent !mkdir ~/.vim/backups > /dev/null 2>&1
" set undodir=~/.vim/backups
" set undofile

" ================ Indentation ======================

set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab
set smartindent
set nofoldenable

" ================ Auto commands ======================

augroup vimrc
    autocmd!
augroup END

augroup module
	autocmd BufRead,BufNewFile *.module set filetype=php
	autocmd BufRead,BufNewFile *.install set filetype=php
	autocmd BufRead,BufNewFile *.test set filetype=php
	autocmd BufRead,BufNewFile *.inc set filetype=php
	autocmd BufRead,BufNewFile *.profile set filetype=php
	autocmd BufRead,BufNewFile *.view set filetype=php
	autocmd BufRead,BufNewFile *.theme set filetype=php
augroup END

" autocmd FileType php set omnifunc=phpcomplete#CompletePHP


call neomake#configure#automake('w')
" autocmd vimrc BufWritePost * Neomake
autocmd vimrc BufWritePre * :call s:StripTrailingWhitespaces()                  "Auto-remove trailing spaces
" autocmd vimrc InsertLeave * NeoSnippetClearMarkers                              "Remove unused markers for snippets

autocmd vimrc FileType html,javascript,coffee,cucumber setlocal sw=2 sts=2 ts=2 "Set 2 indent for html
" autocmd vimrc FileType php,javascript setlocal cc=80                            "Set right margin only for php and js

autocmd vimrc VimEnter * set vb t_vb=

autocmd vimrc BufNewFile,BufReadPost *.md set filetype=markdown                 "Set *.md extension to markdown filetype
" autocmd vimrc FileType nerdtree syntax match hideBracketsInNerdTree
            \ "\]" contained conceal containedin=ALL

" ================ Completion =======================

set wildmode=list:full
set wildignore=*.o,*.obj,*~                                                     "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*cache*
set wildignore+=*logs*
set wildignore+=*node_modules/**
set wildignore+=*DS_Store*
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif
" set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.idea/*,*/.DS_Store,*/vendor,*/node_modules,*/core,*/docker-runtime,*/sites
set wildignore+=.git,.hg,.svn,.idea,.DS_Store,vendor,node_modules,docker-runtime

" ================ Scrolling ========================

set scrolloff=8                                                                 "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=5

" ================ Abbreviations ====================

cnoreabbrev Wq wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qa qa
cnoreabbrev Bd bd
cnoreabbrev bD bd
cnoreabbrev wrap set wrap
cnoreabbrev nowrap set nowrap
cnoreabbrev bda BufOnly
cnoreabbrev t tabe
cnoreabbrev T tabe

" ================ Functions ========================

function! s:StripTrailingWhitespaces()
    let l:l = line(".")
    let l:c = col(".")
    %s/\s\+$//e
    call cursor(l:l, l:c)
endfunction

" Initialize ctrlp plugin for deleting buffers from list
" call ctrlp_bdelete#init()

" ================ Custom mappings ========================

" Comment map
nmap <Leader>c gcc
" Line comment command
xmap <Leader>c gc

" Map save to Ctrl + S
map <c-s> :w<CR>
imap <c-s> <C-o>:w<CR>
" Also save with ,w
" nnoremap <Leader>w :w<CR>

" Easier window navigation
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l
" Open vertical split
nnoremap <Leader>v <C-w>v

" Down is really the next line
nnoremap j gj
nnoremap k gk

" Expand snippets on tab if snippets exists, otherwise do autocompletion
" imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
" \ "\<Plug>(neosnippet_expand_or_jump)"
" \ : pumvisible() ? "\<C-n>" : "\<TAB>"
" " If popup window is visible do autocompletion from back
" imap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<S-Tab>"
" " Fix for jumping over placeholders for neosnippet
" smap <expr><TAB> neosnippet#jumpable() ?
" \ "\<Plug>(neosnippet_jump)"
" \: "\<TAB>"

" Map for Escape key
inoremap jj <Esc>

" Yank to the end of the line
nnoremap Y y$

" Copy to system clipboard
vnoremap <C-c> "+y
" Paste from system clipboard with Ctrl + v
inoremap <C-v> <Esc>"+p
nnoremap <Leader>p "0p

" Move to the end of yanked text after yank and paste
nnoremap p p`]
vnoremap y y`]
vnoremap p p`]

" Move selected lines up and down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Clear search highlight
nnoremap <Leader><space> :noh<CR>

" Handle syntastic error window
nnoremap <Leader>e :lopen<CR>
nnoremap <Leader>q :lclose<CR>

" Toggle between last 2 buffers
nnoremap <leader><tab> <c-^>

" Auto change directory to match current file
nnoremap <Leader>dc :cd %:p:h<CR>:pwd<CR>

" Filesearch plugin map for searching in whole folder
nnoremap <Leader>f :Ack

" Toggle buffer list
" nnoremap <Leader>b :CtrlPBuffer<CR>
" nnoremap <Leader>t :CtrlPBufTag<CR>
" nnoremap <Leader>T :TagbarToggle<CR>
" nnoremap <Leader>m :CtrlPMRU<CR>

" Maps for indentation in normal mode
nnoremap <tab> >>
nnoremap <s-tab> <<

" Indenting in visual mode
xnoremap <s-tab> <gv
xnoremap <tab> >gv

" Resize window with shift + and shift -
nnoremap + <c-w>5>
nnoremap _ <c-w>5<

" Center highlighted search
nnoremap n nzz
nnoremap N Nzz

" ================ plugins setups ========================

let g:ctrlp_match_window = 'bottom,order:ttb,min:1,max:25,results:25'           "Ctrlp window setup
" let g:ctrlp_custom_ignore = {'dir':  '\v[\/]\.(meteor)$'}                       "Ignore .meteor folder

" let g:airline_powerline_fonts = 1                                               "Enable powerline fonts
let g:airline_theme = "dark"                                                  "Set theme to powerline default theme
let g:airline_section_y = '%{(&fenc == "" ? &enc : &fenc)}'                     "set encoding type info
let g:airline_section_z = '%{substitute(getcwd(), expand("$HOME"), "~", "g")}'  "Set relative path
let g:airline#extensions#whitespace#enabled = 0                                 "Disable whitespace extension
let g:airline#extensions#tabline#enabled = 1                                    "Enable tabline extension
let g:airline#extensions#tabline#left_sep = ' '                                 "Left separator for tabline
let g:airline#extensions#tabline#left_alt_sep = '│'                             "Right separator for tabline

let g:gitgutter_realtime = 0                                                    "Disable gitgutter in realtime
let g:gitgutter_eager = 0                                                       "Disable gitgutter to eager load on tab or buffer switch

" let g:user_emmet_expandabbr_key = '<c-e>'                                       "Change trigger emmet key
" let g:user_emmet_next_key = '<c-n>'                                             "Change trigger jump to next for emmet

let g:tagbar_autofocus = 1                                                      "Focus tagbar when opened

" let g:NERDTreeChDirMode = 2                                                     "Always change the root directory
" let g:NERDTreeMinimalUI = 1                                                     "Disable help text and bookmark title
" let g:NERDTreeShowHidden = 1                                                    "Show hidden files in NERDTree
" let g:NERDTreeIgnore=['\.git$', '\.sass-cache$', '\.vagrant', '\.idea']

" let g:neosnippet#disable_runtime_snippets = {'_' : 1}                           "Snippets setup
" let g:neosnippet#snippets_directory = [
"             \ '~/.config/nvim/bundle/vim-snippets/snippets',
"             \ '~/.config/nvim/snippets']

" let g:deoplete#omni_patterns = {}
" let g:deoplete#omni_patterns.php = '\h\w*\|[^. \t]->\%(\h\w*\)\?\|\h\w*::\%(\h\w*\)\?'
" let g:deoplete#enable_at_startup = 1                                            "Enable deoplete autocompletion

let g:ackhighlight = 1                                                          "Highlight current search



let g:neomake_php_phpcs_args_standard = 'PSR2'                                  "Set phpcs to use PSR2 standard
let g:neomake_javascript_enabled_makers = ['eslint']                            "Enable these linters for js

let g:vim_json_syntax_conceal = 0                                               "Disable setting quotes for json syntax

let g:delimitMate_expand_cr = 1                                                 "auto indent on enter

" let g:WebDevIconsNerdTreeAfterGlyphPadding = ' '                                "Set up spacing for sidebar icons



" let g:solarized_termtrans = 1
" set font=Hack:h14
" nnoremap <F4> :YRShow<CR>

" let g:yankring_history_dir = '~/.vim/yankring'

let ctrlp_by_filename = 1

" nnoremap <silent> <F2> :NERDTreeToggle<CR>
" nnoremap <silent> <F6> :CtrlP<CR>
nnoremap <silent> <F7> :CtrlPSmartTabs<CR>
nnoremap <silent> <F8> :CtrlPBuffer<CR>
nnoremap <silent> <F9> :CtrlPFunky<CR>

let g:ctrlp_extensions = ['funky','smarttabs']

let g:ctrlp_custom_ignore = {
  \ 'dir': '\.git$\|cache$\|storage$\|vendor$\|public$',
  \ 'file': '\.txt$\|\.xml$\|.gitignore$'
  \ }

let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Bi-directional find motion
" " Jump to anywhere you want with minimal keystrokes, with just one key
" binding.
" " `s{char}{label}`
" nmap s <Plug>(easymotion-s)
" " or
" " `s{char}{char}{label}`
" " Need one more keystroke, but on average, it may be more comfortable.
nmap s <Plug>(easymotion-s2)
"
"" Turn on case sensitive feature
let g:EasyMotion_smartcase = 1

" call ctrlp_bdelete#init()
"
set relativenumber

autocmd vimrc BufEnter * silent! lcd %:p:h

set termguicolors

let g:session_autosave = 'yes'
let g:session_autoload = 'yes'
" set tags=~/Sites/widgets-drupal/tags
"

let g:split_term_vertical = 'yes'

let g:ackprg = 'ag --nogroup --nocolor --column'

set autoread
au FocusGained * :checktime

nnoremap <silent> <F2> :Vex<CR>
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
let g:netrw_banner = 0
let g:netrw_list_hide = &wildignore
